from . import relays
from . import zones
import json
from os import path
import sys
import time
import datetime
import zmq
from . import config_loader

PORT = 5501


SETTINGS_FILENAME = config_loader.SETTINGS_FILENAME
Day = config_loader.Day  #Alias day

COMMAND_TYPE_SCHEDULED = config_loader.COMMAND_TYPE_SCHEDULED
COMMAND_TYPE_CUSTOM = config_loader.COMMAND_TYPE_CUSTOM
COMMAND_ZONE = config_loader.COMMAND_ZONE
COMMAND_OFF = config_loader.COMMAND_OFF
COMMAND_SKIP = config_loader.COMMAND_SKIP
COMMAND_ZONE_STATUS = config_loader.COMMAND_ZONE_STATUS

__socket = None
__poller = None

waiting_commands = config_loader.waiting_commands  #alias for waiting commands
current_config = config_loader.Settings()  #this is a forward to settings defined below

class CommandException(Exception):
    pass

def send_command(comma_delimited_command) -> str:
    # send a command to a server
    context = zmq.Context()
    socket = context.socket(zmq.PAIR)
    socket.connect("tcp://localhost:%s" % PORT)

    try:
        socket.send_string(comma_delimited_command)

        msg = socket.recv_string()
        if msg.startswith("ERROR"):
            raise CommandException(msg)
        else:
            return msg
    except KeyboardInterrupt:
        return "Received term, exiting..."

def start_server():
    # Start a server listerner for single zone commands
    context = zmq.Context()
    global __socket
    global __poller
    __socket = context.socket(zmq.PAIR)
    __socket.bind("tcp://*:%s" % PORT)

    # Register a poller to listen for incomming events
    __poller = zmq.Poller()
    __poller.register(__socket,zmq.POLLIN|zmq.POLLOUT)

def stop_server():
    if __poller != None:
        __poller.unregister(__socket)

def sleep_loop(seconds):

    end_time = datetime.datetime.now() + datetime.timedelta(seconds=seconds)

    # Loop that handles sleeping, it does not really sleep it waits for network messages/commands
    # look for start requests
    while datetime.datetime.now() < end_time:
        socks = dict(__poller.poll(500))  #poll blockin for up to half a second

        if len(socks) > 0:
            msg = __socket.recv_string()

            command = msg.split(",")
            if command[0] == COMMAND_ZONE:
                if len(command) != 3:
                    __socket.send_string("ERROR: Invalid command format")
                else:
                    try:
                        # Check minutes
                        i = int(command[2])
                        if i < 1 or i > 60:
                            raise ValueError()

                        # Check zone
                        zone = current_config.zone_by_name(command[1])
                        if zone == None:
                            __socket.send_string("ERROR: Unknown zone: " + command[1])
                        elif current_config.get_master_valve() == zone:
                            __socket.send_string("ERROR: Cannot set the state of the master zone " + command[1])
                        else:
                            waiting_commands.append([COMMAND_TYPE_CUSTOM, COMMAND_ZONE, zone, i])
                            __socket.send_string('Queued: start zone "' + zone.get_description( ) + '" for ' + str(i)+ " minutes (" + zone.get_name() + ")")
                            time.sleep(0.5)  #after any command it seems we must sleep else there is some strange error were commands do not run

                    except ValueError:
                        #Handle the exception
                        __socket.send_string("ERROR: Invalid zone on time, duration is minutes between 1 and 60")

            elif command[0] == COMMAND_OFF:
                __socket.send_string("Queued: turn all zones off")
                waiting_commands.clear()
                waiting_commands.insert(0, [COMMAND_TYPE_CUSTOM, COMMAND_OFF])
                time.sleep(0.5)  #after any command it seems we must sleep else there is some strange error were commands do not run
                return  # Return, this cancels all current waits so if a zone is running its wait will end and it will
                        # stop immediately

            elif command[0] == COMMAND_SKIP:
                __socket.send_string("Queued: skip current zone")
                waiting_commands.insert(0, [COMMAND_TYPE_CUSTOM, COMMAND_SKIP])
                time.sleep(0.5)  #after any command it seems we must sleep else there is some strange error were commands do not run
                return  # Return, this cancels all current waits so if a zone is running its wait will end and it will
                        # stop immediately

            elif command[0] == COMMAND_ZONE_STATUS:
                # Find zones that are running we only do one at a time
                on_zone = None
                for zone in current_config.zone_list:
                    if current_config.get_master_valve() == zone:
                        continue
                    elif zone.get_status() == True:
                        on_zone = zone
                        break # found we only run one zone at a time

                if on_zone == None:
                    __socket.send_string("No zones are on")
                else:
                    message = on_zone.get_name() + " remaining minutes: " + str(round((end_time - datetime.datetime.now()).seconds/60))
                    for command in waiting_commands:
                        if command[1] == COMMAND_ZONE:
                            message = message + "\nNext: " + command[2].get_name() + " minutes: " + str(command[3])
                    __socket.send_string(message)
                time.sleep(0.5)  #after any command it seems we must sleep else there is some strange error were commands do not run
            else:
                __socket.send_string("ERROR: Unknown command")



