import RPi.GPIO as GPIO
from enum import Enum

#Define relay names for external use
class Relay(Enum):
    one   = 1
    two   = 2
    three = 3
    four  = 4
    five  = 5
    six   = 6
    seven = 7


#Map physical relay numbers to RPi pin numbers
_relay_index = {'1' : 11,
                '2' : 12,
                '3' : 13,
                '4' : 15,
                '5' : 16,
                '6' : 18,
                '7' : 22 }

def init():
    #Init/prep GPIO for calling
    GPIO.setmode(GPIO.BOARD)
    GPIO.setwarnings(False)  #Do not show pin 11 in use warning

    print("Initializing relays (GPIO)")

    #configure relay connected pins for output
    for pin in _relay_index.items():
        print("  Init relay" + pin[0])
        GPIO.setup(pin[1], GPIO.OUT)

def set_status(relay: Relay, status: bool):
    GPIO.output(_relay_index[str(relay.value)], not status)

def get_status(relay: Relay) -> bool:
    return GPIO.input(_relay_index[str(relay.value)])

def cleanup():
    GPIO.cleanup()
