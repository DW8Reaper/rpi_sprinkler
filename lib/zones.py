from . import relays

import time

USE_SELF_AS_MASTER = "USE_SELF_AS_MASTER"

class Zone:

    __name = ""
    __relay = -1
    __description = ""
    __master_zone = None
    __power_supply = None
    __status = False

    def __init__(self, name, relay: relays.Relay, description, master_zone, power_supply: relays.Relay):
        self.__name = name
        self.__relay = relay
        self.__description = description
        if master_zone == USE_SELF_AS_MASTER:
            self.__master_zone = self
        else:
            self.__master_zone = master_zone
        self.__power_supply = power_supply

    def get_name(self):
        return self.__name;

    def get_relay(self) -> relays.Relay:
        return self.__relay

    def get_description(self):
        return self.__description

    def set_description(self, description):
        self.__description = description

    def set_status(self, status : bool):
        relays.set_status(self.__relay, status)
        self.__status = status

    def get_status(self) -> bool:
        return self.__status
        # DO NOT request relay status this seems to mess up the GPIO guessing you cannot read current state unless it
        #        is and input port
        # return relays.get_status(self.__relay)

