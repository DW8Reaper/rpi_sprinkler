from . import relays
from . import zones
import json
from os import path
import sys
import time
import datetime
from enum import Enum

__author__ = 'dewildt'

SETTINGS_FILENAME = "current_settings.json"
COMMAND_TYPE_SCHEDULED = "scheduled"
COMMAND_TYPE_CUSTOM = "custom"
COMMAND_ZONE = "zone"
COMMAND_OFF = "off"
COMMAND_SKIP = "skip"
COMMAND_ZONE_STATUS = "zone_status"

waiting_commands = []


# Define enum for days, based on %w Weekday as a decimal number [0(Sunday),6].
class Day(Enum):
    sunday = "0"
    monday  = "1"
    tuesday = "2"
    wednesday = "3"
    thursday = "4"
    friday = "5"
    saturday = "6"

class StartEntry:

    __zone = None
    __minutes = 0

    def __init__(self, zone: zones.Zone, minutes : int):
        if minutes > 60:
            sys.exit("Maximum minutes per zone is 60, cannot use " +  str(minutes) + " for zone " + zone.get_name())

        self.__zone = zone
        self.__minutes = minutes

    def get_zone(self) -> zones.Zone:
        return self.__zone

    def get_minutes(self)-> int:
        return self.__minutes

class Start:
    __day = Day.monday
    __time = datetime.time(0, 0, 0)
    __zone_run = None

    __last_run_on = datetime.datetime(1990, 1, 1)

    def __init__(self, day:Day, time: datetime.time):
        self.__day = day
        self.__time = time
        self.__zone_run = []

    def add_entry(self, entry: StartEntry):
        self.__zone_run.append(entry)

    def get_day(self) -> Day:
        return self.__day

    def get_time(self) -> datetime.time:
        return self.__time

    def get_entries(self) -> []:
        return self.__zone_run

    def get_last_run_on(self)-> datetime.datetime:
        return self.__last_run_on

    def run(self):
        self.__last_run_on = datetime.datetime.now()

        for entries in self.__zone_run:
            waiting_commands.append([COMMAND_TYPE_SCHEDULED, COMMAND_ZONE, entries.get_zone(), entries.get_minutes()])

    def skip(self):
        # Skip this start flag completed
        self.__last_run_on = datetime.datetime.now()

class Settings:
    zone_list = None
    starts = None

    __master_valve_name = ""
    __master_valve = None
    __power_supply_relay = None

    def __init__(self):
        self.zone_list = []
        self.starts = []
        if path.isfile(SETTINGS_FILENAME) == True:
            with open(SETTINGS_FILENAME) as data_file:
                data = json.load(data_file)
                self.__master_valve_name = data["master_valve"]
                if data["power_supply"] == "" or data["power_supply"] <= 0:
                    self.__power_supply_relay = None
                else:
                    self.__power_supply_relay = relays.Relay(data["power_supply"])
                self.zone_list = []

                #Create master valve
                self.__master_valve = None
                if self.__master_valve_name != "":
                    for entry in data["zones"]:
                        if entry["name"] == self.__master_valve_name:
                            self.__master_valve = zones.Zone(entry["name"], relays.Relay(entry["relay"]), entry["description"], zones.USE_SELF_AS_MASTER, self.__power_supply_relay)
                            self.zone_list.append(self.__master_valve)
                    if self.__master_valve == None:
                        sys.exit("Could not find the master valve " + self.__master_valve_name + " in the zone list")

                for entry in data["zones"]:
                    if entry["name"] != self.__master_valve_name:
                        self.zone_list.append(zones.Zone(entry["name"], relays.Relay(entry["relay"]), entry["description"], self.__master_valve, self.__power_supply_relay))


                #Get schedule
                for entry in data["schedule"]:
                    # load start details
                    day_str = entry["day"]
                    if day_str.find(",") >= 0:
                        day_str = day_str.split(",")
                    else:
                        day_str = [day_str]

                    for day_value in day_str:
                        day = Day[day_value.strip().lower()]
                        hstr = entry["time"][0:2]
                        mstr = entry["time"][3:5]
                        sstr = entry["time"][6:8]
                        time = datetime.time(int(hstr), int(mstr), int(sstr))

                        # Create start
                        start = Start(day, time)

                        # load entries
                        for run in entry["entries"]:
                            zone = self.zone_by_name(run["name"])
                            if zone == None:
                                sys.exit("Unknown zone name " + run["name"])
                            elif zone == self.__master_valve:
                                sys.exit("Master vavle zone " + run["name"] + " cannot be configured to start ")

                            minutes = run["minutes"]

                            start.add_entry(StartEntry(zone, minutes))

                        if len(start.get_entries()) > 0:
                            self.starts.append(start)

        else:
            self.master_valve = ""
            self.starts = []
            self.zone_list = []

    def has_power_supply(self) -> bool:
        if self.get_power_supply() == None:
            return False
        else:
            return True

    def has_master_valve(self) -> bool:
        if self.get_master_valve() == None:
            return False
        else:
            return True

    def get_power_supply(self) -> relays.Relay:
        return self.__power_supply_relay;

    def get_master_valve(self) -> zones.Zone:
        return self.__master_valve

    def has_zone(self, zone : zones.Zone):
        for z in self.zone_list:
            if z == zone:
                return True
        return False

    def prepare_for_start(self):
        # Before a zone turns on ensure the main power is on
        if self.has_power_supply() == True:
            power_supply = self.get_power_supply()
#            if relays.get_status(power_supply) == False:  #Power supply is off so turn it on
            relays.set_status(power_supply, True)
            time.sleep(1)

        # Now the power is on make sure the main valve is one
        if self.has_master_valve() == True:
            master_valve = self.get_master_valve()
#            if master_valve.get_status( ) == False:
            master_valve.set_status(True)
            time.sleep(1)

    def turn_all_off(self):
        for zone in self.zone_list:
            if zone != self.get_master_valve():
                zone.set_status(False)

        time.sleep(1)
        if self.has_master_valve():
            self.get_master_valve().set_status(False)

        time.sleep(1)
        if self.has_power_supply():
            relays.set_status(self.get_power_supply(), False)

        print("    turned all zones off")


    def zone_by_name(self, name):
        for zone in self.zone_list:
            if zone.get_name() == name:
                return zone

        return None

