#!/usr/bin/python3
import lib.relays as relays
import sys
from pprint import pprint
import lib.config as config
import time
import datetime
import getpass
import os
import argparse
import subprocess
import signal



LOOP_WAIT_SECONDS = 120       # Seconds to wait in each loop so CPU has less work
MAX_GAP_MINUTES = 5         # Max minutes by which a zones start time can be missed

log_file = "/var/log/rpi_sprinkler.log"
error_file = "/var/log/rpi_sprinkler_error.log"
lock_file = os.path.realpath("./.rpi_lock.pid")
closing = False
socket = None  # initially there is not socket
poller = None  # initially ther eis no poller for the socket

# method to get a timestamp for output
def time_stamp():
    return str(datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))

# Method to ensure you are running as the correct user
def check_user():
    if getpass.getuser() != "root":
        exit("Must be run as root to configure RPi output pins")

# Method to get the pid of a running instance else None
def get_running_pid():

    if os.path.exists(lock_file) == True:
        f = open(lock_file, 'r')
        pid = f.readline()
        f.close()
        return pid
    else:
        return None

# method to check if is running
def is_running() -> bool:
    if get_running_pid() == None:
        return False
    else:
        return True

# function to clean log files before we start
def clean_logs():
    f = open(log_file, 'w')
    f.truncate()
    f.close()

    f = open(error_file, 'w')
    f.truncate()
    f.close()

# Method to reserve a pid file
def reserve_pid():
    print("")

    pid = get_running_pid()
    if pid != None:
        exit("Another instance (process " + pid + ") is already running. Lock file " + lock_file)
    else:
        f = open(lock_file, 'w')
        f.write(str(os.getpid()))
        f.close()
        print("Started as process " + str(os.getpid()) + " at " + time_stamp())


# Method to close running instance and perform required cleanup
def close():
    print("")

    config.stop_server()

    if settings != None:
        settings.turn_all_off()

    if os.path.exists(lock_file):
        os.remove(lock_file)

# Method to print configuration
def print_config():
    print("Configuration:")

    if settings.has_master_valve() == True:
        print("  master valve: " + settings.get_master_valve().get_name())

    if settings.has_power_supply() == True:
        print("  power supply: " + str(settings.get_power_supply()))

    print("")

    for zone in settings.zone_list:
        print("  " + zone.get_name() + " - " + str(zone.get_relay()) + " - " + zone.get_description())

def run():
    print("")
    print("Running, CTRL+C to quit")

    config.start_server()
    next_schedule_check = datetime.datetime.now()

    while (True):
        if closing:
            continue

        if datetime.datetime.now() > next_schedule_check:
            next_schedule_check = datetime.datetime.now() + datetime.timedelta(seconds=90)  #Only re-check the schedule every now and then to reduce overhead

            day = config.Day(time.strftime("%w")) # get day of week
            time_now = datetime.time(time.localtime().tm_hour, time.localtime().tm_min,time.localtime().tm_sec)
            for start in settings.starts:
                if start.get_day() != day \
                    or start.get_time() > time_now \
                    or start.get_last_run_on().date() == datetime.datetime.now().date():
                    # Wrong day
                    # Start time not yet reached
                    # already ran today
                    continue

                # Calculate time since this ran
                start_time = start.get_time()
                start_date = datetime.datetime.now()
                start_date = start_date.replace(hour = start_time.hour, minute = start_time.minute, second = start_time.second)
                time_gap = datetime.datetime.now() - start_date

                # Check if too old
                if time_gap.total_seconds() / 60 > MAX_GAP_MINUTES:  #Missed start by 5 minutes skip it altogether
                    print(" " + time_stamp() + " - skip old start")
                    start.skip()
                    continue

                # Run it
                start.run()

        #Run any and all waiting commands
        if len(config.waiting_commands) > 0:
            did_prepare = False
            while len(config.waiting_commands) > 0:
                command = config.waiting_commands.pop(0)
                if command[1] == config.COMMAND_ZONE:
                    zone = command[2]
                    minutes = command[3]
                    message = " " + time_stamp() + " - "
                    if command[0] == config.COMMAND_TYPE_CUSTOM:
                        message = message + "custom start "
                    else:
                        message = message + "strart "
                    message = message + '"' + zone.get_description() + '" for ' + str(minutes) + ' minutes (' + zone.get_name() + ")"

                    print(message)

                    if did_prepare == False:
                        settings.prepare_for_start()
                        did_prepare = True
                    zone.set_status(True)
                    config.sleep_loop(minutes * 60)
                    zone.set_status(False)
                    config.sleep_loop(1)  #always add a small wait between zones

                elif command[1] == config.COMMAND_OFF:
                    print(" " + time_stamp() + " - custom request all off")
                elif command[1] == config.COMMAND_SKIP:
                    print(" " + time_stamp() + " - custom request skip current")

            if did_prepare == True:
                settings.turn_all_off()

        config.sleep_loop(1)


    close()

# Function that will sleep with user feedback for a specific timeframe or until condition is met
def sleep_with_progress(condition, message, max_time):
    # Print intro message
    if message != "":
        sys.stdout.write(message)
        sys.stdout.flush()

    # wait required seconds or till condition
    for x in range(1,max_time * 2):
        sys.stdout.write(".")
        sys.stdout.flush()
        time.sleep(0.5)  # refresh every half a second
        if condition():
            break

# Function to handle termination signals
def handle_close(sig, frame):
    if sig == signal.SIGINT or sig == signal.SIGTERM:
        print("Received signal to close...")
        closing = True
        close()
        print("")
        exit(0)

# Helper to do checks and run a command
def do_command(command, pause = True):
    # Make sure running in bacground
    if is_running() == False:
        exit("Main process is not currently running, unabled to continue")
    else:
        try:
            result = config.send_command(command)
            print(result)
            if pause:
                time.sleep(1)  # prevent too many simultaneous commands

        except config.CommandException as error:
            exit(error)

        except KeyboardInterrupt:
            print("Received term, exiting")

def check_running_full():
    pid = get_running_pid()
    if pid == None:
        print("Not running")
    else:
        try:
            os.kill(int(pid), 0)
        except OSError:
            print("Was running as PID " + pid + " but has been cancelled (lock file exists)")
        else:
            print("Running PID: " + pid)

# Print/configure arguments for this program
args_parser = argparse.ArgumentParser(description="Sprinkler controller program. This program runs a sprinkler schedule based on the provided configuration."
    + " Must be run as root since this is required by the RPi API for GPIO" )

#args_parser.add_argument('--target_path', dest="path", type=str, required=True,
#                        help= "Required, directory in the GIT repository that as the corresponding directory that is being merged" )
args_parser.add_argument('--config', dest="config", action='store_const', const=True, required=False, default=False, help="Print configuration" )

args_parser.add_argument('-b', dest="background", action='store_const', const=True, required=False, default=False, help="Start/run in background" )

args_parser.add_argument('--status', dest="status", action='store_const', const=True, required=False, default=False, help="Check status if already running" )

args_parser.add_argument('--zone_status', dest="zone_status", action='store_const', const=True, required=False, default=False, help="Check status of zones if running" )

args_parser.add_argument('--stop', dest="stop", action='store_const', const=True, required=False, default=False, help="Stop running process" )

args_parser.add_argument('--off', dest="all_off", action='store_const', const=True, required=False, default=False, help="Turn off all zones" )

args_parser.add_argument('--skip', dest="skip", action='store_const', const=True, required=False, default=False, help="Skip current running zone if any" )

args_parser.add_argument('--zone_on', dest="zone_on", type=str, required=False, default="", \
                        help= "Turn on the specified zone name" )

args_parser.add_argument('--minutes', dest="minutes", type=int, required=False, default=-1, \
                        help= "Number of minutes to turn a zone on for" )

args = args_parser.parse_args()

if args.status == True:
    # User requested the status, see if we are still running
    check_running_full()

elif args.zone_status == True:
    # users requested the status of current zones
    pid = get_running_pid()
    if pid == None:
        print("Not running")
    else:
        do_command(config.COMMAND_ZONE_STATUS, False)

elif args.stop == True:
    # User requested we stop the process
    check_user()  # Ensure running as root

    pid = get_running_pid()
    if pid == None:
        print("Not running")  # already stopped
    else:
        os.kill(int(pid), signal.SIGTERM)
        sleep_with_progress(lambda: True if get_running_pid() == None else False, "Stopping process " + pid, 15)

        print("")

        pid = get_running_pid()
        if pid != None:
            exit("failed to close")
        else:
            exit(0)

elif args.background == True:
    # User requested we start the process in background
    check_user()  # Ensure running as root

    if is_running() == True:
        exit("Already running...")
    else:
        command = "sudo python3 -u " + sys.argv[0] + "  >" + log_file + " 2>" + error_file + " & "
        process = subprocess.Popen([command], shell=True)
        process.communicate()

        sleep_with_progress(lambda: True if get_running_pid() != None else False, "Starting", 15)

        time.sleep(1)
        check_running_full()

elif args.skip == True:
    # User requested we skip the current action if any
    do_command(config.COMMAND_SKIP)

elif args.all_off == True:
    # User requested we start the process in background
    do_command(config.COMMAND_OFF)

elif args.config:
    # Print config
    settings = config.current_config
    print_config()

elif args.zone_on != "":
    # Make sure running in bacground
    if args.minutes < 1 or args.minutes > 60:
        exit("Did not provide minutes the zone must be on for")
    else:
        do_command(config.COMMAND_ZONE + "," + args.zone_on + "," + str(args.minutes))

else:
    # Run the sprinkler schedule
    print("")
    check_user()  # Ensure running as root
    
    # prepare for start
    clean_logs()
    reserve_pid()
    relays.init()

    # load configuration
    settings = config.current_config
    print_config()

    # register process signal handler
    signal.signal(signal.SIGTERM, handle_close)
    signal.signal(signal.SIGINT, handle_close)

    try:
        # run
        run()
    except SystemExit:
        # Normal exit nothing to do here
        exit(0)
    except:
        print("Unexpected error:", sys.exc_info()[0])
        close()
        raise







